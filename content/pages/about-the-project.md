---
title: About the Project
featuredImage: /archive/media/lookout.png
featuredImageCaption: Photo Caption
---
Please join the VHS Archives Working Group for a public unveiling and participatory party where our light-weight video archiving tool will be demoed and then played with by all.

We will screen clips from two videos through our ANALOGUE ARCHIVES tool built by Partner & Partners:
* Clips from Gay Cable Network, Gay USA, September 20,1990 (selected by Kyle Croft, from Visual AIDS)
* Clips from Santa Cruz Women's Media Collective, International Videoletters project, 1975 (selected by Helena Shaskevich, CUNY GC, Art History)
We will add material items to a paper timeline that will then be digitized in front of our eyes!
There will be a critical making project sponsored by 3Text Studio, a Doctoral and Graduate Students' Council (DSC) Chartered Organization.

Feel free to bring an item that reminds you of the Gay 90s, or the feminist 70s, including but not limited to fliers, letters, photos, books, buttons, or feelings.

The VHS Archives Working Group brings together scholars, students, librarians, archivists, technologists and community members interested in discussing questions, concerns and best practices about the use, preservation, digitization, and research of VHS collections currently held by organizations, scholars, artists, and activists. For two years member of the working group have presented their archive and the questions, difficulties, surprises, losses and bounty it holds. The group has used each test case as a fertile opportunity to better frame their own video archives, the broader questions raised by holdings of media nearing format-obsolescence, and the building of a tool that will allow others to work with the vulnerable analogue archives of political people.
